# SmartContract - Voting

Le code se situe dans le fichier "Voting.sol" et un exemple d'utilisation se trouve dans l'enregistrement d'écran dans ce même repo, ou à l'adresse suivante : https://www.youtube.com/watch?v=RCgixETlMJ8

## Fonction réalisées : 

- L'administrateur du vote inscrit une liste blanche d'électeurs identifiés par leur adresse Ethereum.
- L'administrateur du vote démarre la session d'enregistrement des propositions.
Les électeurs inscrits peuvent soumettre leurs propositions pendant que la session d'enregistrement est active.
- L'administrateur du vote clôture la session d'enregistrement des propositions.
- L'administrateur du vote lance la session de vote.
- Les électeurs inscrits votent pour leur proposition favorite.
- L'administrateur du vote clôture la session de vote.
- L'administrateur du vote comptabilise les votes.
- Tout le monde peut vérifier les derniers détails de la proposition gagnante.

## Fonctions additionnelles

- Le poid d'un votant : on peut imaginer que le vote d'un votant peut être plus influant en fonction de son rôle ou de son statut au sein de la campagne. 
- Une méthode permettant d'afficher toutes les descriptions des propositions faites
