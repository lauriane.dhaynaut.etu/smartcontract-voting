// SPDX-License-Identifier: GPL-3.0

import "@openzeppelin/contracts/access/Ownable.sol";

pragma solidity 0.8.21;


contract Voting is Ownable {

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
        uint weight;
    }   

    struct Proposal {
            string description;
            uint voteCount;
    }

    mapping (address => Voter) public voters ;

    Proposal [] public proposals ;

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    WorkflowStatus public workflowStatus ;

    uint winningProposalId ; 

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted (address voter, uint proposalId);

    constructor(uint _weight) Ownable (msg.sender) {
        workflowStatus = WorkflowStatus.RegisteringVoters;
        voters[msg.sender].isRegistered=true;
        voters[msg.sender].weight=_weight;
    } 

    function registeringVoters (address _ownerAddress, uint _weight) external onlyOwner {
        require(workflowStatus==WorkflowStatus.RegisteringVoters, "Vous n'etes pas a la bonne etape");
        require(!voters[_ownerAddress].isRegistered, "Le votant a deja ete enregistre." );
        voters[_ownerAddress].isRegistered=true;
        voters[_ownerAddress].weight=_weight;
        emit VoterRegistered(_ownerAddress);
    }

    function startProposalRegistration () external onlyOwner{
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;
    }

    function stopProposalRegistration () external onlyOwner {
        workflowStatus = WorkflowStatus.ProposalsRegistrationEnded ; 
    }

    function addProposal (string memory proposalDesc ) external {
        require(workflowStatus==WorkflowStatus.ProposalsRegistrationStarted, "Vous n'etes pas a la bonne etape");
        require(voters[msg.sender].isRegistered, "Vous ne faites pas partis de la liste blanche");
        Proposal memory proposal ;
        proposal.description = proposalDesc ; 
        proposal.voteCount = 0 ; 
        proposals.push(proposal);
        emit ProposalRegistered(proposals.length-1);
    }

    function startVote () external onlyOwner {
        workflowStatus = WorkflowStatus.VotingSessionStarted ;
    }

    function stopVote () external onlyOwner {
        workflowStatus = WorkflowStatus.VotingSessionEnded ;
    }

    function vote (uint idProposal) external  {
        require(workflowStatus==WorkflowStatus.VotingSessionStarted, "Vous n'etes pas a la bonne etape"); 
        Voter storage voter = voters[msg.sender];
        require(voters[msg.sender].isRegistered, "Vous ne faites pas partis de la liste blanche");
        require(!voter.hasVoted, "Vous avez deja vote");
        proposals[idProposal].voteCount+=voter.weight;
        voter.hasVoted=true;
        voter.votedProposalId=idProposal;
        emit Voted(msg.sender, idProposal);
    }

    function countingVotes () external onlyOwner {
        require(workflowStatus==WorkflowStatus.VotingSessionEnded, "Vous n'etes pas a la bonne etape") ; 
        uint maxVotes = 0 ;
        for(uint i=0 ; i<proposals.length ; i++) {
            if(maxVotes<proposals[i].voteCount){
                maxVotes=proposals[i].voteCount;
                winningProposalId=i;
            }
        }
        workflowStatus=WorkflowStatus.VotesTallied;
    }

    function getWinner() public view returns (uint winningProposalId_) {
        require(workflowStatus==WorkflowStatus.VotesTallied, "Vous n'etes pas a la bonne etape");
        winningProposalId_=winningProposalId;
    }

    function getProposals() public view returns (string[] memory descriptions) {
        descriptions = new string[](proposals.length);
        for (uint256 i = 0; i < proposals.length; i++) {
            descriptions[i] = proposals[i].description;
        }
    }
}